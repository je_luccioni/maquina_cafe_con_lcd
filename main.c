/**
* 
* *********************************************************************************//**
* \addtogroup maquina_cafe_lcd
* @{ borreame -> }@
* \copyright  
* 2020, Luccioni Jesús Emanuel \n
* All rights reserved.\n 
* This file is part of main.c .\n
* Redistribution is not allowed on binary and source forms, with or without \n
* modification. Use is permitted with prior authorization by the copyright holder. &copy;
* \file main.c
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.    
* \brief Descripcion breve.
* \details Descripcion detallada.
* 
* \version 01v01d01.
* \date Sabado 24 de Octubre, 2020.
* \pre pre, condiciones que deben cuplirse antes del llamado, example: First
* initialize the system.
* \bug bug, depuracion example: Not all memory is freed when deleting an object 
* of this class.
* \warning mensaje de precaucion, warning.
* \note nota.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* @} doxygen end group definition 
* ********************************************************************************** */

/* 
 * 
* ==================================================================================================
* 
* ***********************************[ begin, header file include ]***********************************
* 
* ==================================================================================================
* 
*/
#define __main_c__ /**<@brief Etiqueta para marcar el archivo fuente como principal main */

#include "config_word.h"



#include <xc.h>
#include <pic18f4550.h>
#include "main.h"

/* 
* 
* ==================================================================================================
* 
* ***********************************[ End,   header file include ]***********************************
* 
* ==================================================================================================
*/
#define __switch_ENABLE__   0 /**<@brief Habilitamos '1'/Deshabilitamos '0' el Uso de los Switch */


/* 
* 
* ==================================================================================================
* 
* ********************************[ End,   declaracion de funciones ]*******************************
* 
* ==================================================================================================
*/

/* 
 * 
* ==================================================================================================
* 
* ****************************[ begin, declaracion variables globales ]***************************
* 
* ==================================================================================================
*/
/**
 * \var systic_acuTick;
 * \brief Variable global, del tipo volatile (ya que es actualizada en 
 * el ISR del timer seleccionado como fuente de tick).
 * \details  
 * \warning Esta variable no debe ser editada por ninguna funcion, solamente 
 * puede leerse y no editarse.
 * \date Jueves 22 de Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i> 
* <b><i> piero.jel@gmail.com </i></b>. */
volatile tick_t systic_acuTick[2];

/**
* \var Tecla;
* \brief Variable donde se almacena la Tecla Presionada
* , del tipo \ref uint8_t.
* \version 01v01d02.
* \note .
* \warning .
* \date Jueves 22 de Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>
* <b><i> piero.jel@gmail.com </i></b>. */
uint8_t Tecla; 

/**
* \var Moneda;
* \brief Varible Global en la que se almacena la moneda aceptada 
* por el dispositivo Monedero, del tipo \ref uint8_t.
* \version 01v01d01
* \note nota.
* \warning mensaje de "warning".
* \date Jueves 22 de Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>
* <b><i> piero.jel@gmail.com </i></b>. */
uint8_t Moneda;




/* -- Definimos la Tablas, con alcance global */
/**
* \var tbl_moneda
* \brief Tabla que contiene el valor asignado a las diferentes 
* monedas que acepta el sistema.
* \warning Esta es solo usada desde la funcion keypad_update(). 
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
  * <b><i> piero.jel@gmail.com </i></b>.
  * \version 01v01d01.
*/
 const uint16_t tbl_moneda[4] = {\
        MONEDA0_SALDO,
        MONEDA1_SALDO,
        MONEDA2_SALDO,
        MONEDA3_SALDO };

/* 
* 
* ==================================================================================================
* 
* ****************************[ End,   declaracion variables globales ]****************************
* 
* ==================================================================================================
*/





    


/* 
* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* 
* ===========================[ begin, ISR (Rutinas para el servicio de Interupciones),  ]===========================
* 
* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* 
*/

/* 
* ==================================================================================================
* 
* *****************[ begin, ISR systick Interrupt Service Routine for Timer0  ]********************
* 
* ==================================================================================================
* 
*/
/**
*\def TIMER_PREload
* \details
* \brief Etiqueta que referencia el valor de recarga del TIMER0,
* Parte baja.
* \note
* \warning
*/
#define TIMER_PREload 0x06

/**
* \fn void  __interrupt () Timer0_ISR();
* \brief Funcion para el servicio de interupcion por Desborde  
* del Timer 0, esta es de indole privada. No debe ser llamada desde
* ningun lugar.
* \return nothing
* 
* \note Esta funcion pertenece a un vector y no debe ser invocada. El 
* Procesador salta a el cuando el evento configurado ocurre.
* \warning Esta funcion no debe ser invocada desde ningun sector 
* de codigo, es un trap de eventos de interupcion.
*/
void  __interrupt () Timer0_ISR()
{    
    TMR0L = TIMER_PREload;    
    systic_acuTick[TICK_ms]++;
    if(systic_acuTick[TICK_ms] == 100)
        systic_acuTick[TICK_ms] = 0, systic_acuTick[TICK_100ms]++;    
    INTCONbits.T0IF = 0; /* Bajamos la bandera de  interrupciones    */
}
/* 
* 
* ==================================================================================================
* 
* *****************[ End  , ISR systick Interrupt Service Routine for Timer0  ]********************
* 
* ==================================================================================================
*/

/* 
* 
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* 
* ===========================[ End,   ISR (Rutinas para el servicio de Interupciones),  ]===========================
* 
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/



/* 
* 
* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* 
* =======================================[ begin, funcion main() "prinicpal" ]=======================================
* 
* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* 
*/




/**
* \fn void main(void);
* \details descricpion de tallada
* \brief Funcion principal del proyecto. Esta es invodada por el 
* StartUp del Ssitema.
* \return nothing.
* \note 
* \warning 
*/
void main(void)
{
    /*-- configuramos el tick del sistema */    
    systick_Init();    
    /*-- Configurmamos las Interupciones */
    CONFIG_INTERRUPT();
    /* *****************************************************************
     * Inicializacion del harware que usaremos
     */
    /* inicializamos el led de blink, p/monitoreo
     * del funcionameinto del Sistema  */
    blinky_init();
    /* Establecemos el numero de blinks, en caso
     * de querer debugear podemos con esta api 
     * establecer otros valores, para el blink. */
    blinky_set(2);
    //switch_init();
    /*-- Configuramos e inicializamos el LCD*/    
    lcd_init();
    lcd_clear();      
    /* inicializamos el teclado matricial */                
    keypad_init();
    /* icializamos el monedero */
    monedero_init();
    /* icializamos la maquina de estado principal */
    maquina_cafe_init();    
    while(1)
    {     
        /* Atualizamos la FSM del Blink Led */
        blinky_update();
        /*-- Update la FSM de monitoreo de Teclado. */
        Tecla = keypad_update();
        if(MONEDERO) /* Si el Monedero esta habilitado 
            lo encuestamo*/
        {
            Moneda = monedero_update();    
        }        
        /*-- Esta actualizacion necesita los datos Previos 
         *  + Tecla
         *  + Moneda
         */
        maquina_cafe_update();
        
        
        
       
    }
    return;
}





/* 
* 
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* 
* =======================================[ End,   funcion main() "prinicpal" ]=======================================
* 
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/







/* 
*
* ==================================================================================================
* 
* ********************************[ begin, declaracion funciones  ]********************************
* 
* ==================================================================================================
* 
*/

/*
 * \fn void systick_Init(void);
 * \brief funcion para inicializar el systick del sistema. Inicializa el 
 * Timer seleccionado como fuente de tick. 
 * \return nothing. 
 * \note
 */
void systick_Init(void)
{
    
/*-- 
 *  OSCCON: Registro de control del Oscilador
 _______________________________________________________________________________________________
|           |           |           |           |           |           |           |           |   
| IDLEN     | IRCF2     | IRCF1     | IRCF0     | OSTS      | IOFS      | SCS1      | SCS0      |
|___________|___________|___________|___________|___________|___________|___________|___________|
 * 
 * IDLEN, Bit de Habilitacion del modo IDLE
 *      IDLEN = 1 , Dispositivo entra en Modo IDLE  con la instruccion SLEEP
 *      IDLEN = 0 , Dispositivo entra en Modo SLEEP con la instruccion SLEEP 
 * 
 *  >> IDLEN = 0 , deshabilitamos el modo IDLE al ejecutar la Instruccion Sleep.    
 *  
 * IRCF2 | IRCF1 | IRCF0 : Bits de seleccion para la frecuencia del Oscilador Interno 
 *      111 = 8 MHz (INTOSC drives clock directly)
 *      110 = 4 MHz
 *      101 = 2 MHz
 *      100 = 1 MHz (3)
 *      011 = 500 kHz
 *      010 = 250 kHz
 *      001 = 125 kHz
 *      000 = 31 kHz (from either INTOSC/256 or INTRC directly) (2) 
 *  >> IRCF2 | IRCF1 | IRCF0 | = 111 , seleccionamso el Oscilador interno de 8 MHz
 * 
 * OSTS, Bit de Estado de Time-Out Start-Up del Oscilador.
 *      OSTS = 1,   Oscillator Start-up Timer time-out has expired; primary oscillator is running
 *      OSTS = 0,   Oscillator Start-up Timer time-out is running; primary oscillator is not ready
 *  >> OSTS = 0, Es un bit de estado para lectura.
 * 
 * IOFS, INTOSC Frequency Stable bit
 *      IOFS = 1,   La frecuencia INTOSC es estable
 *      IOFS = 0,   La frecuencia INTOSC no es estable
 *  >> IOFS = 0, Es un bit de estado para lectura.
 * 
 * SCS1 | SCS0 : Bits para la seleccion del Clock del Sistema
 *      1x = Oscilador Interno
 *      01 = Oscilador Uno Timer 1
 *      00 = Oscilador Primario
 *  >> SCS1 = 1, Oscilador Interno.
 * 
 * OSCCON = 0b01110010 = 0x72
 */  
    OSCCON = 0x72;
/*-- 1- Configuracion del Timer, manteniendolo apagado
 *  Tipo 8-Bit preescaler 8
 _______________________________________________________________________________________________
|           |           |           |           |           |           |           |           |   
| TMR0ON    | T08BIT    | T0CS      | T0SE      | PSA       | T0PS2     | T0PS1     | T0PS0     |
|___________|___________|___________|___________|___________|___________|___________|___________|
 * 
 * TMR0ON  = 0  , Lo encendemos luego de configurar la Interrupciones
 * T08BIT  = 1  , TIMER0 de 8Bits
 * T0CS    = 0  , Fuente de clock interna 8 Mhz
 * T0SE    = 0  , No tine inportancia ya que "T0CS = 0"
 * PSA     = 0  , Asignamos el preescaler al TIMER0
 * Preescaler = 8, 
 * T0PS2   = 0
 * T0PS1   = 1
 * T0PS0   = 0
 * 
 * -- Configuracion de las Interrupciones para el TMR0  INTCON 
 * INTCON <TMR0IF>  = 0  , Ponemos en cero el flag de Interrupcion por overfload del TMR0
 * INTCON <TMR0IE>  = 1  , Habilitamso la Interrupcion por Overflad del TMR0
 */     
    T0CON = 0b01000010;
    TMR0L = 0x06;
    INTCONbits.T0IF = 0;
    INTCONbits.T0IE = 1;
    T0CONbits.TMR0ON = 1;    
}
/*
 * \fn uint8_t systick_TimeOut( tick_t *pacu,const tick_t time);
 * \brief funcion para chequear si transcurrio un lapso de tiempo, en caso de 
 * que el tiempo se vencio el acumulador es actualizado.
 * \param pacu : Puntero al acumulador local, que pertenece a la funcion.
 * \param time : valor en mili segundo del lapso del tiempo.  
 * \return 
 *      + 0, el lapso del tiempo se vencio y el acumulador se actualizo
 *      + 1, el lapso de tiempo no se vencio y el acumulador no fue actualizado.
 * \note antes de usar por primera ves esta API debemos asegurarnos que el 
 * acumulador que usaremos como referencia se halla inicializado correctamente.
 * 
 */
uint8_t systick_TimeOut( tick_t *pacu,const tick_t time,tickType_t type)
{
    tick_t tmp;
    //if(!time) return 0;
	tmp = systic_acuTick[type] - *pacu;
    /* if(tmp > time) */
	if(tmp >= time)
	{
        *pacu = systic_acuTick[type];
        return 0;
	}
	return 1;
}
/*
 * \fn void systick_pause(tick_t time,tickType_t type);
 * \brief funcion para pausar un numero de mili segundos 
 * \param time : valor en mili segundo del lapso del tiempo.  
 * \param type : para especificar el tipo de demora. Este puede ser:
 *   \li \ref TICK_ms, captura de mili segundos.
 *   \li \ref TICK_100ms, captura de centenas de mili segundos. * 
 * \return 
 *      + 0, el lapso del tiempo se vencio y el acumulador se actualizo
 *      + 1, el lapso de tiempo no se vencio y el acumulador no fue actualizado.
 * \note antes de usar por primera ves esta API debemos asegurarnos que el 
 * acumulador que usaremos como referencia se halla inicializado correctamente.
 * \warning */
void systick_pausems(tick_t time)
{
    tick_t tmp;
    static tick_t acu_tmp;
    acu_tmp = systic_acuTick[TICK_ms];
    do
    {
        tmp = systic_acuTick[TICK_ms] - acu_tmp;
    }while(tmp < time);
}

/*
* \fn void blinky_update(uint8_t nBlink);
* \details descricpion de tallada
* \brief funcion para actualizar o establecer la FSM blink. 
* \param nBlink : Mediante este argumento podemos establecer el blink
* o en su defecto solo actualizar la FSM.
*   + \ref blinky_UPDATE : solo se actualiza.
*   + Valor entre 0 y 7, establece el blink. Si pasamos un valor mayor 
*   este es casteado a un maximo de 7.
* \return nothing
*/
void blinky_update_FSM(uint8_t nBlink)
{
    static tick_t blinky_acums;
    static uint8_t status, blink,i;        
    if(nBlink != blinky_UPDATE )
    {
        status = 0;
        blink = nBlink & 0x0F;
        return;
    }
    switch(status)    
	{
	default:
	case 0:
        LED1(OFF);
		i = blink << 0x01;
        blinky_acums = systick_capture(TICK_100ms);
        break;
	case 1:        
		while(i)
		{
            if(systick_TimeOut(&blinky_acums,blinky_TIME_ON,TICK_100ms)) return;
            LED1(TOGGLE);
            i -= 1;
		}
        if(systick_TimeOut(&blinky_acums,blinky_TIME_OFF,TICK_100ms)) return;
        status = 0;                
		return;
	}
	status += 1;
}






#if (__switch_ENABLE__ == 1)
void switch_init(void)
{
    /*-- port C, configiramos la parte alta  como INPUT*/\
    PORTC &= 0x0F;   /*-- Clean de escritura de estado de pines */\
    LATC &= 0x0F;    /*-- Clean de lectura de estado de pines */\
    UCON &= 0B11110111;     /* UCON<3> = 0 */\
    UCFG |= 0B00001000;     /* UCFG<3> = 1 */\
    TRISC |= 0xF0;   /*-- RC7~RC4 -> Input */\
}
/*
* \fn uint8_t switch_readSW(uint8_t nSw);
* \details
* \brief Fucnion que se encarga de leer el estado de los Switch.
* \param nSw : Numero del Pulsador que se desea leer.
* \return Este retorna el estado del \ref nSw.
*   + 1 , El Switch esta en estado Alto.
*   + 0 , El Switch esta en estado Bajo.
* \note 
* \warning El valor retornado de esta dependera del Pull de los Switch.
* \par example 
<pre>
</pre>
*/
uint8_t switch_readSW(uint8_t nSw)
{
    switch(nSw)
    {
        default:
        case switch_Up:
            return switch_SW_UP();
        case switch_Down:
            return switch_SW_DOWN();
        case switch_Aceptar:
            return switch_SW_ACEPTAR();
        case switch_Volver:
            return switch_SW_VOLVER();            
    }    
    return 0;
}
/*
* \fn uint8_t switch_update(uint8_t nSw));
* \details 
* \brief Funcion para actualizar la FSM para Switch. En caso de detectar
* la presion retorna el valor del SW o 0.
* \param nSw : Numero de SW que deseamos actualizar.
* \return El valor del SW presinado o switch_NOT_PUSH
*   + 0 se presiono el Switch por el cual consultamos.
*   + switch_NOT_PUSH, en caso de no detectar un pulsador presionado
*/
uint8_t switch_update(uint8_t nSw)
{
    static tick_t acu_tickms[4];
    static uint8_t status[4];
	switch(status[nSw])
	{
	default:
        status[nSw] = 0;
	case 0:
		/*-- escaneamos para detectar un flanco ascendente */
		if(!switch_readSW(nSw))
		{
			acu_tickms[nSw] = systick_capture(TICK_ms);
            break;
		}
		return switch_NOT_PUSH;
	case 1:
		/*-- esperamos hasta agotar el debounce time  */
		if(systick_TimeOut(&acu_tickms[nSw],switch_NOISE_TIME,TICK_ms)) 
            return switch_NOT_PUSH;
		break;
	case 2:
		/*-- consultamos si se mantiene el estado del sw */
		if(!switch_readSW(nSw)) break;
        else 
		{
            /*-- ruido en la linea */
            status[nSw] = 0;
            return switch_NOT_PUSH ;
		}        
	case 3:
		/*-- esperamos hasta que se suelte el pulsador */
		if(switch_readSW(nSw))
		{
            acu_tickms[nSw] = systick_capture(TICK_ms);
            break;
		}
		return switch_NOT_PUSH ;
	case 4:
		/*-- esperamos hasta agotar el debounce time  */
		if(systick_TimeOut(&acu_tickms[nSw],switch_NOISE_TIME,TICK_ms)) 
            return switch_NOT_PUSH;
		break;        
	case 5:
		/*-- consultamos si se mantiene el estado del sw */
		if(switch_readSW(nSw))
		{
			/*-- reiniciamos la FSM*/
			status[nSw] = 0;
			return 0;
		}
		/*-- no se pudo validar el estado */
        status[nSw] = 3 ;
		return switch_NOT_PUSH;
	}
	status[nSw] += 1;
    return switch_NOT_PUSH;    
}
#endif


/*-- Si configuramos el LCD con bus de Datos de 4-Bits
 * No se puede leer el mismo, por lo que tener el Pin
 * R/W habilitado es desperdiciar un Pin. Se debe conectar este
 * a GND en el modulo LCD, para que siempre este configurado como
 * entrada (aceptacion de datos) el bus de datos del LCD.
 */

#define RS  LATDbits.LD7 /**<@brief Establecemos el Pin que usaremos para Habilitar/Deshabilitar el envio de Instruccion/dato al Modulo LCD */
#define EN  LATDbits.LD5 /**<@brief Establecemos el Pin que usaremos para Habilitar/Deshabilitar el Modulo LCD */

void lcd_init(void)
{         
    TRISD &= 0x01;       /*PORT as Output Port*/
    
    systick_pausems(15);        /*15ms,16x2 LCD Power on delay*/
    // -- Iniciamos la secuencia de comandos a enviar                                    
    lcd_Command(0x02);  /*send for initialization of LCD with nibble method */
	lcd_Command(0x28);  /*use 2 line and initialize 5*7 matrix in (4-bit mode)*/
    lcd_Command(0x01);  /*clear display screen*/
	lcd_Command(0x0c);  /*display on cursor off*/ 
    lcd_Command(0x06);  /*increment cursor (shift cursor to right)*/
}


#define MASK_nLCD   0b11100001    /**<@brief Mascara para limpiar los pines del Puerto usados como bus de datos para el Modulo LCD*/
#define MASK_LCD    0b00011110    /**<@brief Mascara para Cargar lso datos al Puerto usados como bus de datos para el Modulo LCD*/

void lcd_Command(unsigned char cmd )
{
    uint8_t tmp;
    /*-- 
     * 1° Enviamos la parte Alta del cmd 
     */
    
    //cmd >>= 0x03;
    tmp = cmd >> 3;
    RS = 0;  /* Habilitamos el envio de Instruccion*/ 
	/*Limpiamos los BITS del Nibble alto */
    LATD &= MASK_nLCD;   /* 0b11100001 */ 
	LATD |= (MASK_LCD & tmp); /* 0b00011110 */ /*Send higher nibble of command first to PORT*/ 
	/*-- Creamos el Ciclo de Aceptacion del primer Nibble*/
	EN = 1;  
	NOP();
	EN = 0;    
	systick_pausems(1);
    tmp = cmd << 1;    
    /*Limpiamos los BITS del Nibble alto */
    LATD &= MASK_nLCD;    /* 0b11100001 */  
	LATD |= (MASK_LCD & tmp); /* 0b00011110 */ /*Send higher nibble of command first to PORT*/ 	
	/*-- Creamos el Ciclo de Aceptacion del primer Nibble*/
	EN = 1;  
	NOP();
	EN = 0; 
    systick_pausems(3);
}


void lcd_putc(unsigned char cmd)
{    
    uint8_t tmp;
    /*-- 
     * 1° Enviamos la parte Alta del cmd 
     */
    
    //cmd >>= 0x03;
    tmp = cmd >> 3;
    RS = 1;  /* Habilitamos el envio de Instruccion*/ 
	/*Limpiamos los BITS del Nibble alto */
    LATD &= MASK_nLCD;   /* 0b11100001 */ 
	LATD |= (MASK_LCD & tmp); /* 0b00011110 */ /*Send higher nibble of command first to PORT*/ 
	/*-- Creamos el Ciclo de Aceptacion del primer Nibble*/
	EN = 1;  
	NOP();
	EN = 0;    
	systick_pausems(1);
    tmp = cmd << 1;    
    /*Limpiamos los BITS del Nibble alto */
    LATD &= MASK_nLCD;    /* 0b11100001 */  
	LATD |= (MASK_LCD & tmp); /* 0b00011110 */ /*Send higher nibble of command first to PORT*/ 	
	/*-- Creamos el Ciclo de Aceptacion del primer Nibble*/
	EN = 1;  
	NOP();
	EN = 0; 
    systick_pausems(3);
}

void lcd_puts(const char *str)
{
	while((*str)!=0)
	{		
	  lcd_putc(*str);
	  str++;
    }
}

void lcd_puts_xy(unsigned char posX,unsigned char posY,const char *msg)
{    
    posX &= 0x0F;    
    posX |= 0x80;
    if(posY == 1) posX |= 0x40;
    //
    lcd_Command(posX);
    lcd_puts(msg);
}

void lcd_clear()
{
    lcd_Command(0x01);  /*clear display screen*/
    systick_pausems(3);
}


void lcd_puts_saldo(uint8_t posX, uint8_t posY,\
    uint16_t saldo)
{
    
    uint8_t buff[6];
    buff[0] = 0x30,buff[1] = 0x30;
    buff[3] = 0x30,buff[4] = 0x30;
    buff[2] = ',';
    buff[5] = '\0';
    if(saldo > 9999) saldo = 9999;
    while(saldo >= 1000)
    {
        saldo -= 1000;
        buff[0]++;
    }
    while(saldo >= 100)
    {
        saldo -= 100;
        buff[1]++;        
    }
    while(saldo >= 10)
    {
        saldo -= 10;
        buff[3]++;        
    }   
    buff[4] += (uint8_t) (saldo);
    lcd_puts_xy(posX,posY,(const char*)buff);   
}

/**
* \var keypad_map
* \brief Variable constante que almacena el mapa del teclado.
* \warning Esta es solo usada desde la funcion keypad_update(). 
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
  * <b><i> piero.jel@gmail.com </i></b>.
  * \version 01v01d01.
*/
uint8_t keypad_map[4][3] = { \
     { 0 , 1 , 2 }
    ,{ 3 , 4 , 5 }
    ,{ 6 , 7 , 8 }
    ,{keypad_NOT_KEYPRESS,keypad_NOT_KEYPRESS,keypad_NOT_KEYPRESS}
};

/**
 * \fn void keypad_shifterCol(uint8_t ncol);
 * \brief Funcion para realizar el shifter de la columna.
 * \param ncol : Columna que debemos habilitar o hasta donde tenemos
 * que desplazarnos.
 * \return nothing  */
void keypad_shifterCol(uint8_t ncol)
{
    // 1- apagamos todos las columnas
    LATB |= 0x07;
    switch(ncol)
    {
        case 0:
            LATB &= 0b11111110;
            return;
        case 1:
            LATB &= 0b11111101;
            return;
        case 2:
            LATB &= 0b11111011;
            return;  
    }
}
/**
 * \fn void keypad_init(void);
 * \brief
 * \return nothing
 */
void keypad_init(void)
{
    /*-- haemos el clean antes de reconfigurar el puerto */
    PORTB = 0; 
    LATB = 0;
    /*-- Parte alta como Input, Parte Baja como salida */
    TRISB = 0xF0;
}


/**
* \fn uint8_t keypad_update(void);
* \details
* \brief Funcion que actualiza la fsm de teclado.
* \return Retorna el valor de la tecla presionada, correspondiente con el 
* mapa de memoria. En en caso detectar una presion retorna 
* \ref keypad_NOT_KEYPRESS
* \note nota sobre la funcion
* \warning concideraciones a tener en cuenta para manejar con precaucion
* \par example 
<pre>
</pre>
*/
uint8_t keypad_update(void)
{
    static tick_t acu_tickms ;
    static uint8_t status, keypress;
    uint8_t row,col;
    
    if(systick_TimeOut(&acu_tickms,10,TICK_ms))
        return keypad_NOT_KEYPRESS;    
    // Ingresamos en la FSM    
	switch(status)
	{
	default:
        status = 0;
	case 0:                
		/*-- escaneamos para detectar un flanco ascendente */
        col = 0;
        while(col < 3)
        {
            keypad_shifterCol(col);
            row = PORTB & 0xF0;
            if(row == 0xF0) col++;
            else break;                
        }        
        if(row == 0xF0) return keypad_NOT_KEYPRESS; 
        //
        keypress = col;
        keypress |= row;
        // capturo el tiempo
        acu_tickms = systick_capture(TICK_ms);        
        status = 1;
        return keypad_NOT_KEYPRESS;        
    case 1:
        /*-- esperamos hasta agotar el debounce time  */
        // -- volvemos a leer las Filas
		row = PORTB & 0xF0; 
        col = keypress & 0xF0;
        if(row != col)
        {
            status = 0 /* El estado de la Fila cambio antes de agotarse el Debounce Time */;            
        }
        else status = 2 /* Se valido la presion de la tecla  */;        
        return keypad_NOT_KEYPRESS;                
    case 2:        
        // -- leemos las filas para ver si se solto la tecla
		row = PORTB & 0xF0;
        if(row != 0xF0)
        {
            //-- Sigue la Tecla presionada                        
            return keypad_NOT_KEYPRESS;
        }
        //se solto la tecla presionada
        // capturamos el tiempo 
        acu_tickms = systick_capture(TICK_ms);        
        status = 3;
        return keypad_NOT_KEYPRESS;        
    case 3:
        /*-- esperamos hasta agotar el debounce time  */
        // -- leemos de nuevo la fila para asegurarnos de que no sea ruido
        row = PORTB & 0xF0;
        if(row != 0xF0)
        {
            // tenemos ruido en la linea, iniciamos la FSM                        
            status = 2;
            return keypad_NOT_KEYPRESS;            
        }
        // se solto la tecla presionada, validamos la misma.        
        col = keypress & 0x0F;
        switch(keypress & 0xF0)
        {
            case 0B01110000:
                row = 3;
                break;
            case 0B10110000:
                row = 2;
                break;
            case 0B11010000:
                row = 1;
                break;
            case 0B11100000:            
                row = 0;
                break;
        }        
        keypress = keypad_map[row][col]; 
        /*-- Reiniciamos la Maquina de Estado. */
        status = 0;
        return keypress;                 
    }
}



/**
* \var menu_productos
* \brief Tabla que contiene el menu de producto en formato String
* y hasta 16 caracteres. Para que se visualicen de forma correcta en el modulo LCD.
* \warning La tabla de String es alamacenada en memoria de programa.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>
* <b><i> piero.jel@gmail.com </i></b>.
* \version 01v01d01.
*/
menu_sT menu_productos[9] = {
    /*"0123456789ABCDEF" */
      "Cafe solo amargo", /**<@brief Item 0, Producto Cafe Solo, Amargo */
      "Cafe solo dulce ", /* 1 */
      "Cafe c/l amargo ", /* 2 */
      "Cafe c/l dulce  ", /* 3 */
      "Cafe c/l dulce+ ", /* 4 */
      "Capuchino amargo", /* 5 */
      "Capuchino dulce ", /* 6 */
      "   Te amargo    ", /* 7 */
      "    Te dulce    ", /* 8 */    
};

/* puede ser de 8-bit ya que los presios son todos 
 del tipo redondeado*/
/**
* \var tbl_saldo
* \brief Tabla que contiene el saldo de producto en entero 16-bits, esto
* quiere decir que el monto esta multiplicado por 100.
* \warning La tabla de String es alamacenada en memoria de programa.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
* <b><i> piero.jel@gmail.com </i></b>.
* \version 01v01d01.
*/
const uint16_t tbl_saldo[9] = { 500,600,800,1000,1100,1200,1500,800,1000};
/**
* \var tbl_time_act
* \brief Tabla que contiene el tiempo que necesita estar activo el actuador
* ,con la conbinacion enviad, en funcion con el producto seleccionado.
* \warning La tabla de String es alamacenada en memoria de programa.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
* <b><i> piero.jel@gmail.com </i></b>.
* \version 01v01d01.
*/
const tick_t tbl_time_act[9] = { 150,150,250,250,250,150,250,150,150};


void maquina_cafe_init(void)
{
    
#if 0 /* No puedo usar RC5 y RC4 como salida */
        /* Configuramos los Pines de RE para I/= Digitales,
    los desenganchamos del Modulo ADC 
    TRISE &= 0B11111001; Configuracion de I/O digitales            
            * RE2   -> OUTPUT
            * RE1   -> OUTPUT
            * RE0   -> OUTPUT    
    Por defecto configuramos todos los Pines relacionados al
    ADC y Modulo Comparador como Digital.
            */        
    ADCON1 |= 0B00001111;
    CMCON |= 0x07; 

#endif     

    /*-- Configuracion el Puerto C, I/O Digital [Actuador y Monedero] */    
    PORTC &= 0b11111000;   /*-- Clean de escritura de estado de pines */\
    LATC &= 0b11111000;    /*-- Clean de lectura de estado de pines */\
    UCON &= 0B11110111;     /* UCON<3> = 0 */
    UCFG |= 0B00001000;     /* UCFG<3> = 1 */
    /* Configuramos los Pines Del Actruador como Salida*/    
    ACTURADOR0_CFG = 0;
    ACTURADOR1_CFG = 0;
    ACTURADOR2_CFG = 0;
    
    
}

void maquina_cafe_update(void)
{
    static uint8_t  status=0;
    static tick_t time,timeAct /*tiempo del Actuador*/;
    /*static tick_t time2*/
    /*al agragar la atrea del teclado en el
    estado 2, cada ves que cambie la tarifa debemos esperar nuevanten*/
    /*-- para obtener una demora en leer el monedero */
    /*/static tick_t timeMonedero,timeMonTmp;*/
    /**/
    static uint16_t Saldo;
    static uint8_t tecla;
    /*bool_t viewSaldo = FALSE;*/
    switch(status)
    {
        default:
            status = 0;
        case 0:           /*"0123456789abcdef"*/
            lcd_puts_xy(0,0,"Presione Tecla  ");
            lcd_puts_xy(0,1,"P/selec. Product");
            break;            
        case 1:
            /*-- monitoreamos el teclado*/
            if(Tecla == keypad_NOT_KEYPRESS)
                return;
            /*-- salvamos localmente la tecla presionada */
            tecla = Tecla;
            /*-- salvamos el saldo a cobrar */
            Saldo = tbl_saldo[tecla];
            /*-- salvamos el tiempo que debe estar
             * activo el modulo actuador
             */
            timeAct = tbl_time_act[tecla];
            lcd_puts_xy(0,0,menu_productos[tecla].str);
                          /*"0123456789abcdef" */
            lcd_puts_xy(0,1,"Importe: $      ");
            lcd_puts_saldo(sizeof("Importe: $"),1,Saldo);
            /* capturo el tiempo */            
            time = systick_capture(TICK_100ms);
            /*-- capturamos el tiempo para el acumulador que usaremos en el monedero */
            /*timeMonedero = systick_capture(TICK_100ms);
            timeMonTmp = 0;*/
            /* Encendemos un Monedero */
            MONEDERO = 1;
            /* salimos */
            break;            
        case 2:            
            if(!systick_TimeOut(&time,MONEDERO_TIME_ENABLE /* 10 Seg */,TICK_100ms)) 
            {
                /* Apagamos el monedero */
                MONEDERO = 0;
                status = 0;
                return;
            }  
#if 1
            /*-- para cumplir con el paso tres mantenemos habilitado el teclado */
            if(Tecla != keypad_NOT_KEYPRESS)
            {
                /* Volvemos a slavar localmente el valor de la tecla presionada */
                tecla = Tecla;
                /*-- salvamos el saldo a cobrar */
                Saldo = tbl_saldo[tecla];
                /*-- salvamos el tiempo para el Actuador */
                timeAct = tbl_time_act[tecla];
                /* Como se presionao una tecla, 
                 * estimamos que paso un timpo y
                 * es prudente consultar por el monedero
                 * por un tiempo estimado al principio.
                 * Para ello vuelvo a capturar un time, ya que la tarifa
                 * se actualizao, lo correcto es volver a esperar 10 segundo
                 * por el ingreso de las monedas.
                 */
                time = systick_capture(TICK_100ms);
                /* Como cambio la tarifa debemos habilitar 
                 * que se muestre esta de nuevo junto con la leyenda
                 * del producto*/                
                lcd_puts_xy(0,0,menu_productos[tecla].str);
                lcd_puts_saldo(sizeof("Importe: $"),1,Saldo);
                
            }
#endif            
               
/**/            
#if 1            
            /*-- Monitoreo del monedero */
            if( Moneda != MONEDERO_EMPTLY)
            {
                if(Saldo < tbl_moneda[Moneda])                
                    Saldo = 0;          
                /*
                 * podemos colocar un bloque y dentro de el
                 * junto con Saldo = 0;
                 * break; para pasar al siguente estado de la FSM
                 * */
                else Saldo -=  tbl_moneda[Moneda];
                /**/
                lcd_puts_saldo(sizeof("Importe: $"),1,Saldo);
            }
#else            
        /*
         * Debemos limpiar los datos que usamos para esta seccion, 
         * como los acumuladores. Si es que no son untilizados en otro lugar.
         *      >> timeMonTmp
         *      >> timeMonedero
         *      >> viewSaldo
         */
            /*-- para observar la accion del monedero ya que nos se expresa el tiempo 
             * de estado bajo del pin correspondiente. Lo ideal es consideral los pines 
             * como Switch
             */
            if(systick_TimeOut(&timeMonedero,timeMonTmp,TICK_100ms)) return;
            if(!MONEDA0)
            {
                if(Saldo < MONEDA0_SALDO)                
                    Saldo = 0;          
                else Saldo -=  MONEDA0_SALDO;
                /**/
                viewSaldo = TRUE;
            }
            if(!MONEDA1)
            {
                if(Saldo < MONEDA1_SALDO)              
                    Saldo = 0;
                else Saldo -= MONEDA1_SALDO;
                viewSaldo = TRUE;
            }
            if(!MONEDA2)
            {
                if(Saldo < MONEDA2_SALDO)
                    Saldo = 0;
                else Saldo -= MONEDA2_SALDO;
                viewSaldo = TRUE;
            }
            if(!MONEDA3)
            {
                if(Saldo < MONEDA3_SALDO) Saldo = 0;                               
                else Saldo -= MONEDA3_SALDO;
                viewSaldo = TRUE;
            }
            /*-- Consultamos si debemos mostrar los cambios en saldo*/
            if(viewSaldo)
            {
                lcd_puts_saldo(sizeof("Importe: $"),1,Saldo);
                viewSaldo = FALSE;
            }
#endif            

            /*-- Consultamos por el saldo */
            if( Saldo == 0) break;                
            //status = 0;
            //timeMonTmp = TIME_WAIT_MONEDERO;
            return;      
        case 3:          
            /*
             * Apagamos el Monedero
             * Y mostramos el Mesnaje de waits
             * Con el saldo en la Segunda linea
             */            
            MONEDERO = 0;            
            Saldo = tbl_saldo[tecla];
            lcd_puts_xy(0,0,"Aguarde Pre. Ped");
            lcd_puts_xy(0,1,"Pago Ok: $      ");
            lcd_puts_saldo(sizeof("Importe: $"),1,Saldo);
            /*-- debemos llamar a actuador con la tecla presionada */
            maquina_cafe_actuador(tecla);
            /*-- Capturamos el Tiempo para el Acumulador */
            time =systick_capture(TICK_100ms);
            break;
        case 4:            
            /*-- esperamos mientras prepara el producto */
            if(systick_TimeOut(&time,timeAct,TICK_100ms)) return;   
            /*-- apagamos el actuador */
            maquina_cafe_actuador(ACTUADOR_OFF);
            /*-- Preparamos el contexto para la espera de 20 Seg*/
            time =systick_capture(TICK_100ms);
            break;
        case 5:           /*"0123456789aBCDEF"*/
            lcd_puts_xy(0,0,"Espere Sirviendo");
            lcd_puts_xy(0,1,"    Producto    " );
            break;
        case 6:
            if(systick_TimeOut(&time,200/* Wait 20 Seg*/,TICK_100ms)) return; 
            status = 0;
            return;
            
    }    
    status++;       
}


void maquina_cafe_actuador(uint8_t action)
{
    switch(action)
    {
        default:
        case ACTUADOR_OFF:
            ACTURADOR0 = 0;
            ACTURADOR1 = 0;
            ACTURADOR2 = 0;
            return;
        case 0:
            
            ACTURADOR0 = 0;
            ACTURADOR1 = 0;
            ACTURADOR2 = 1;
            
            return;
        case 1:
            
            ACTURADOR0 = 1;
            ACTURADOR1 = 0;
            ACTURADOR2 = 1;
            
            return;
        case 2:
            
            ACTURADOR0 = 0;
            ACTURADOR1 = 1;
            ACTURADOR2 = 1;
            
            return;
        case 3:
            
            ACTURADOR0 = 1;
            ACTURADOR1 = 1;
            ACTURADOR2 = 1;
            return;
        case 4:
            
            ACTURADOR0 = 1;
            ACTURADOR1 = 1;
            ACTURADOR2 = 1;
            return;
        case 5:
            
            ACTURADOR0 = 0;
            ACTURADOR1 = 1;
            ACTURADOR2 = 1;
            return;
        case 6:
            
            ACTURADOR0 = 1;
            ACTURADOR1 = 1;
            ACTURADOR2 = 1;
            return;
        case 7:
            
            ACTURADOR0 = 0;
            ACTURADOR1 = 0;
            ACTURADOR2 = 0;
            return;
        case 8:
            
            ACTURADOR0 = 1;
            ACTURADOR1 = 0;
            ACTURADOR2 = 0;
            return;

        
        
    }
    
}

void monedero_init(void)
{
    /* Configuramos los Pines Del Monedero como entradas
     * Puertos PORTA, PORTC y PORTD
     * 
     * SALIDAS:
     *  RA2
     * 
     * ENTRADAS:
     *  RD0
     *  RC2
     *  RC1
     *  RC0     */
    

    /* Configuramos los Pines Multiplexados con el 
     * Modulo ADC y del Comparador como I/O Digital.  */        
    ADCON1 |= 0B00001111;
    CMCON |= 0x07; 
    /*-- configuramos el monedero */
    MONEDERO_CFG = 0;
    /* debemos verificar la configuracion del pueerto C, como entrada */
    MONEDA0_CFG = 1;
    MONEDA1_CFG = 1;
    MONEDA2_CFG = 1;
    MONEDA3_CFG = 1;
    /*-- Iniciamos con el monedero apagado */
    MONEDERO = 0;
}


        
uint8_t monedero_update(void)
{
    /* para manejar el tiempo entre un ingreso y el siguente */
    static tick_t time = 0;       
#if 1    
    static uint8_t status, rval;
        
    switch(status)
    {
        case 0:
            /*Leemos los Pines del Monedero*/
            if(!MONEDA0) 
                {rval = MONEDA0_INDEX; break;}
            if(!MONEDA1) 
                {rval = MONEDA1_INDEX; break;}
            if(!MONEDA2)
                {rval = MONEDA2_INDEX; break;}
            if(!MONEDA3)
                {rval = MONEDA3_INDEX; break;}
        case 1:
            if(systick_TimeOut(&time,TIME_WAIT_MONEDERO,TICK_ms))
                return MONEDERO_EMPTLY;
            switch(rval)
            {
                case MONEDA0_INDEX:
                    if(!MONEDA0) 
                    {
                        status++;
                        return MONEDERO_EMPTLY;
                    }
                    break;
                case MONEDA1_INDEX:
                    if(!MONEDA1) 
                    {
                        status++;
                        return MONEDERO_EMPTLY;
                    }
                    break;
                case MONEDA2_INDEX:
                    if(!MONEDA2) 
                    {
                        status++;
                        return MONEDERO_EMPTLY;
                    }
                    break;
                case MONEDA3_INDEX:
                    if(!MONEDA3) 
                    {
                        status++;
                        return MONEDERO_EMPTLY;
                    }
                    break;
                
            }
            status = 0;
            return MONEDERO_EMPTLY;
        case 2:
            switch(rval)
            {
                case MONEDA0_INDEX:
                    if(MONEDA0) break;
                    else return MONEDERO_EMPTLY;

                case MONEDA1_INDEX:
                    if(MONEDA1) break;
                    return MONEDERO_EMPTLY;
                    
                case MONEDA2_INDEX:
                    if(MONEDA2) break;                    
                    return MONEDERO_EMPTLY;
                    
                case MONEDA3_INDEX:
                    if(MONEDA3) break;
                    return MONEDERO_EMPTLY;               
            }
            time = systick_capture(TICK_ms);
            break;
        case 3:
            if(systick_TimeOut(&time,TIME_WAIT_MONEDERO,TICK_ms))
                return MONEDERO_EMPTLY;
            switch(rval)
            {
                case MONEDA0_INDEX:
                    if(MONEDA0) 
                    {
                        status++;
                        return MONEDERO_EMPTLY;
                    }
                    break;
                case MONEDA1_INDEX:
                    if(MONEDA1) 
                    {
                        status++;
                        return MONEDERO_EMPTLY;
                    }
                    break;
                case MONEDA2_INDEX:
                    if(MONEDA2) 
                    {
                        status++;
                        return MONEDERO_EMPTLY;
                    }
                    break;
                case MONEDA3_INDEX:
                    if(MONEDA3) 
                    {
                        status++;
                        return MONEDERO_EMPTLY;
                    }
                    break;
                
            }
            status = 2;
            return MONEDERO_EMPTLY;
        case 4:
            status = 0;
            return rval;        
    }
    status++;
    return MONEDERO_EMPTLY;
#else    
    if(systick_TimeOut(&time,TIME_WAIT_MONEDERO,TICK_ms))
        return MONEDERO_EMPTLY;   
    /*-- Monitoreo del monedero */
    if(!MONEDA0) return MONEDA0_INDEX;
    if(!MONEDA1) return MONEDA1_INDEX;
    if(!MONEDA2) return MONEDA2_INDEX;
    if(!MONEDA3) return MONEDA3_INDEX;
    return MONEDERO_EMPTLY;  
#endif
}

/* 
* 
* ==================================================================================================
* 
* ********************************[ End,   declaracion funciones  ]********************************
* 
* ==================================================================================================
*/
