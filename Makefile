#
#  There exist several targets which are by default empty and which can be 
#  used for execution of your targets. These targets are usually executed 
#  before and after some main targets. They are: 
#
#     .build-pre:              called before 'build' target
#     .build-post:             called after 'build' target
#     .clean-pre:              called before 'clean' target
#     .clean-post:             called after 'clean' target
#     .clobber-pre:            called before 'clobber' target
#     .clobber-post:           called after 'clobber' target
#     .all-pre:                called before 'all' target
#     .all-post:               called after 'all' target
#     .help-pre:               called before 'help' target
#     .help-post:              called after 'help' target
#
#  Targets beginning with '.' are not intended to be called on their own.
#
#  Main targets can be executed directly, and they are:
#  
#     build                    build a specific configuration
#     clean                    remove built files from a configuration
#     clobber                  remove all built files
#     all                      build all configurations
#     help                     print help mesage
#  
#  Targets .build-impl, .clean-impl, .clobber-impl, .all-impl, and
#  .help-impl are implemented in nbproject/makefile-impl.mk.
#
#  Available make variables:
#
#     CND_BASEDIR                base directory for relative paths
#     CND_DISTDIR                default top distribution directory (build artifacts)
#     CND_BUILDDIR               default top build directory (object files, ...)
#     CONF                       name of current configuration
#     CND_ARTIFACT_DIR_${CONF}   directory of build artifact (current configuration)
#     CND_ARTIFACT_NAME_${CONF}  name of build artifact (current configuration)
#     CND_ARTIFACT_PATH_${CONF}  path to build artifact (current configuration)
#     CND_PACKAGE_DIR_${CONF}    directory of package (current configuration)
#     CND_PACKAGE_NAME_${CONF}   name of package (current configuration)
#     CND_PACKAGE_PATH_${CONF}   path to package (current configuration)
#
# NOCDDL
##################################################################################
##
## -- definimos el nombre del proyecto/directorio que lo contiene
## 
##################################################################################
## Los Archivos Que contienen el nombre del proyecto
## modificado:     nbproject/private/private.xml
## modificado:     nbproject/project.xml
### Donde se registran cambios al modificar nombres de archivos 
### o agregar y quitar 
## modificado:     nbproject/Package-default.bash
## modificado:     nbproject/configurations.xml
## modificado:     nbproject/private/private.xml
## modificado:     nbproject/project.xml


NAME_PROJECT=maquina_cafe_lcd_02



##


# Environment 
MKDIR=mkdir
CP=cp
CCADMIN=CCadmin
RANLIB=ranlib


# build
build: .build-post

.build-pre:
# Add your pre 'build' code here...

.build-post: .build-impl
# Add your post 'build' code here...


# clean
clean: .clean-post

.clean-pre:
# Add your pre 'clean' code here...
# WARNING: the IDE does not call this target since it takes a long time to
# simply run make. Instead, the IDE removes the configuration directories
# under build and dist directly without calling make.
# This target is left here so people can do a clean when running a clean
# outside the IDE.

.clean-post: .clean-impl
# Add your post 'clean' code here...


# clobber
clobber: .clobber-post

.clobber-pre:
# Add your pre 'clobber' code here...

.clobber-post: .clobber-impl
# Add your post 'clobber' code here...


# all
all: .all-post

.all-pre:
# Add your pre 'all' code here...

.all-post: .all-impl
# Add your post 'all' code here...


# help
help: .help-post

.help-pre:
# Add your pre 'help' code here...

.help-post: .help-impl
# Add your post 'help' code here...

##
## Editando el Makefile: 
##	Agregando el Target new
##
new: 
	@make clean --no-print-directory
	@make all --no-print-directory


# include project implementation makefile
include nbproject/Makefile-impl.mk

# include project make variables
include nbproject/Makefile-variables.mk


DOXY_FILE := Doxyfile
DOC_FLAG := -s
DOC_VIEW := google-chrome
doc: doc_clean
	@echo "\n==============================[begin, target doc]==============================\n"
	@doxygen $(DOC_FLAG) ./$(DOXY_FILE)
	@echo "==============================[ End , target doc ]==============================\n"
	
	
doc_clean:
	@echo "\n==============================[begin, target clean_doc]==============================\n"
	@rm -fR ./doc/* 
	@echo "==============================[ End , target clean_doc ]==============================\n"

doc_create:
	@echo "\n==============================[begin, target doc_create]==============================\n"
	@echo "doxygen $(DOC_FLAG) -g ./$(DOXY_FILE)"
#	doxygen $(DOC_FLAG) -g ./$(DOXY_FILE)
	@echo "\n==============================[ End , target doc_create ]==============================\n"
	
doc_open: doc
	@echo "\n==============================[begin, target run_doc]==============================\n"
	@echo "opening index.html ..... "
	@$(DOC_VIEW) ./doc/html/index.html
	@echo "\n==============================[ End , target doc_create ]==============================\n

doc_help:
	@echo "\n==============================[begin, target help]==============================\n"
	@printf "\n\nSintaxis (Uso):\n\n"	
	@printf "\t\033[33mImportante: Debemos tener instalado \033[32mdoxygen\033[33m en nuesto sistema.\033[0m\n"
	@printf "\t\t En linux:\033[36m sudo apt-get install doxygen\033[0m\n"
	@printf "\t\t En windows, download unpack 7Zip and install:\033[36m https://sourceforge.net/projects/doxygen/files/snapshots/\033[0m\n"
	@printf "\tmake\033[36m doc\033[0m Genera la documentacion\n"
	@printf "\tmake\033[36m doc_clean\033[0m Genera la documentacion\n"
	@printf "\tmake\033[36m doc_create\033[0m Genera la documentacion\n"
	@printf "\tmake\033[36m doc_open\033[0m Limpia la documentacion, la crea de nuevo\n"
	@printf "\ty abre la misma con navegador establecido en la variable \n"
	@printf "\t\"DOC_VIEW = $(DOC_VIEW)\", establecida en el archivo \"\033[32mproject.mk\033[0m\"\n"
	@printf "\tPodemos establecerlo al llamar a \"make\" de la siguente manera:\n"
	@printf "\t\t\033[36mmake doc_open DOC_VIEW="firefox"\033[0m\n"
	@printf "\tmake\033[36m doc_help\033[0m Muestra el mensaje de ayuda de la docuemtnacion solamente\n"	
	@echo "==============================[ end, target help ]==============================\n"
	
PUSH:
	@echo "\n==============================[begin, target git PUSH]==============================\n"
	@./git_script push
	@echo "\n==============================[ End , target git PUSH ]==============================\n"
	

COMMIT:
	@echo "\n==============================[begin, target git COMMIT]==============================\n"
	@./git_script commit
	@echo "\n==============================[ End , target git COMMIT ]==============================\n"

LOG:
	@echo "\n==============================[begin, target git LOG]==============================\n"
	@./git_script log
	@echo "\n==============================[ End , target git LOG ]==============================\n"
