/**
* 
* *********************************************************************************//**
* \addtogroup maquina_cafe_lcd
* @{ 
* \copyright  
* 2020, Luccioni Jesús Emanuel \n
* All rights reserved.\n 
* This file is part of main.h .\n
* Redistribution is not allowed on binary and source forms, with or without \n
* modification. Use is permitted with prior authorization by the copyright holder. &copy;
* \file main.h
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.    
* \brief Descripcion breve.
* \details Descripcion detallada.
*
* \version 01v01d01.
* \date  19 de Octubre, 2020.
* \pre pre, condiciones que deben cuplirse antes del llamado, example: First
* initialize the system.
* \bug bug, depuracion example: Not all memory is freed when deleting an object 
* of this class.
* \warning mensaje de precaucion, warning.
* \note nota.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* @} doxygen end group definition 
* ********************************************************************************** */


/*
* 
* ==================================================================================================
* 
* **************************[ begin, redefinicion de datos            ]***************************
* 
* ==================================================================================================
* 
* 
*/
 
/**
* \typedef uint8_t;
* \brief tipo de datos de 8-Bits sin signo idem a "unsigned char".
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \version 01v01d01
* \warning */ typedef unsigned char uint8_t;
/**
  * \typedef int8_t ;
  * \brief tipo de datos de 8-Bits con signo idem a "char" o "singned char"
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
  * <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
  * \version 01v01d01.  
  * \warning */ typedef signed char int8_t;  
/**
  * \typedef uint16_t;
  * \brief redefinicion de tipo de dato entero de 16-Bits.
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
  * <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
  * \version 01v01d01  
  * \warning */ typedef unsigned short uint16_t;/**<@brief tipo de datos de 16-Bits sin signo idem a "unsigned short"*/
/**
  * \typedef int16_t;
  * \brief tipo de datos de 16-Bits con signo idem a "short, int" o "signed short, signed int"
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
  * <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
  * \version 01v01d01    
  * \warning */ typedef signed short int16_t;
/**         
  * \typedef uint32_t;
  * \brief redefinicio tipo de datos de 32-Bits sin signo idem a "unsigned long"
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
  * <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
  * \version 01v01d01 
  * \warning */ typedef unsigned long uint32_t; 
/**       
  * \typedef int32_t;
  * \brief Redefinicion tipo de datos de 32-Bits con signo idem a "long" o "signed long"
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
  * <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
  * \version 01v01d01   
  * \warning */ typedef signed long int32_t;   
/**     
  * \typedef tick_t;
  * \brief redefinicion del tipo de dato para usar como tick del sistema
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
  * <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
  * \version 01v01d01   
  * \warning */ typedef uint8_t tick_t; 

/**
* \enum bool_t
* \brief redefinicion de tipo de dato enumeracion para boolean.
* \li \ref FALSE
* \li \ref TRUE 
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \version 01v01d01 */
typedef enum
{
    FALSE = 0,      /**<@brief enumeracion para establecer el valor logico FALSE '0'. */
    TRUE = !FALSE,  /**<@brief enumeracion para establecer el valor logico TRUE '!0', distinto de cero. */
} bool_t;


/**
* \enum tickType_t
* \brief enumeracion para establecer estados logicos
* \li \ref TICK_ms
* \li \ref TICK_100ms
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \version 01v01d01. */
typedef enum
{
    TICK_ms = 0,   /**<@brief enumeracion para seleccionar tick de mili segundos. */
    TICK_100ms = 1,/**<@brief enumeracion para seleccionar tick de 100ms "centenas de milisegundos". */    
//    TICK_10s = 2,/**<@brief enumeracion para seleccionar tick de 10s "decenas de milisegundos". */
} tickType_t;

/**
* \struct menu_sT
* \brief Redefinicion de una estructura de dato para almacenar un menu
* conformado por string.
* \li \ref str
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \version 01v01d01. */
typedef struct 
{
    const char * str;/**<@brief string que forma parte del menu. */   
}menu_sT;
/* 
* 
* ==================================================================================================
* 
* ***************************[ End,   redefinicion de datos            ]***************************
* 
* ==================================================================================================
*/


/* 
* ==================================================================================================
* 
* *************************[ begin, macros etiquetas de configuracion ]*************************
* 
* ==================================================================================================
* 
*/


#define LED1_PULL_UP    1 /**<@brief Habilitasmo '1'/Deshabilitamos '0' la configuracion 
                                     Pull Up del LED1 */
#define SIMULL_ENABLE   1 /**<@brief Habilitasmo '1'/Deshabilitamos '0' la configuracion 
                                     Simulacion del Proyecto (acotar los tiempos). */

                                     
#define blinky_TIME_ON  3 /**<@brief Establecemos el tiempo de encendido, para el LED1, en 200 ms */
#define blinky_TIME_OFF 7 /**<@brief Establecemos el tiempo de encendido, para el LED1, en 800 ms */
/**
*\def blinky_UPDATE
* \details
* \brief Mediante esta etiqueta le especificamos a la funcion que actualiza la
* FSM de Blink, que debe actualizar los estados y no el valor del Blink.
* \note
* \warning */
#define blinky_UPDATE   0xEE /**<@brief Medinate este especificamso que se tiene que actualizar 
la fsm de blink */




/* 
* 
* ==================================================================================================
* 
* *************************[ End,   macros etiquetas de configuracion ]*************************
* 
* ==================================================================================================
*/






/* 
 * 
* ==================================================================================================
* 
* *******************************[ begin, declaracion de funciones ]********************************
* 
* ==================================================================================================
*/


/* 
* --------------------------------------------------------------------------------------------------
* 
* ***********************************[ begin, systick_timer ]***********************************
* 
* --------------------------------------------------------------------------------------------------
* TODO systick_timer
*/
/**
  * \fn uint8_t systick_TimeOut( tick_t *pacu,const tick_t time,tickType_t type);
  * \brief funcion para chequear si transcurrio un lapso de tiempo, en caso de 
  * que el tiempo se vencio el acumulador es actualizado.
  * \param pacu : Puntero al acumulador local, que pertenece a la funcion.
  * \param time : valor en mili segundo del lapso del tiempo.  
  * \param type : para especificar el tipo de demora. Este puede ser:
  *   \li \ref TICK_ms, captura de mili segundos.
  *   \li \ref TICK_100ms, captura de centenas de mili segundos.
  * 
  * \return 
  *      + 0, el lapso del tiempo se vencio y el acumulador se actualizo
  *      + 1, el lapso de tiempo no se vencio y el acumulador no fue actualizado.
  * \note antes de usar por primera ves esta API debemos asegurarnos que el 
  * acumulador que usaremos como referencia se halla inicializado correctamente.
  *\author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
  * <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
  * \version 01v01d01.
 */
uint8_t systick_TimeOut( tick_t *pacu,const tick_t time,tickType_t type);

/**
  * \fn void systick_Init(void);
  * \brief funcion para inicializar el systick del sistema. Inicializa el 
  * Timer seleccionado como fuente de tick. 
  * \return nothing. 
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
  * <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
  * \version 01v01d01.
 */
void systick_Init(void);

/**
 * \fn void systick_pausems(tick_t time);
 * \brief funcion para pausar un numero de mili segundos 
 * \param time : valor en mili segundo del lapso del tiempo.   
 * \return 
 *      + 0, el lapso del tiempo se vencio y el acumulador se actualizo
 *      + 1, el lapso de tiempo no se vencio y el acumulador no fue actualizado.
 * \note antes de usar por primera ves esta API debemos asegurarnos que el 
 * acumulador que usaremos como referencia se halla inicializado correctamente.
 * \warning
 * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
  * <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
  * \version 01v01d01.
 */
void systick_pausems(tick_t time);
/* 
 * 
 * --------------------------------------------------------------------------------------------------
 * 
 * ***********************************[ End,   systick_timer ]***********************************
 * 
 * --------------------------------------------------------------------------------------------------
 */


/* 
* --------------------------------------------------------------------------------------------------
* 
* ***********************************[ begin, led_status ]***********************************
* 
* --------------------------------------------------------------------------------------------------
* 
*/
/*
 * Establecemos el Pin donde colocaremos el PIN designado 
 * el Led Blinky
 *
 */

/**
 * \def PLED1
 * \brief Macro para Establecer el Pin relacionado a \ref blinky_update()
 * \ref blinky_init() \ref blinky_set().
 * \return nothing
 * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
 * <b><i> piero.jel@gmail.com </i></b>.
 * \version 01v01d01.
 * */
#define PLED1        LATBbits.LB3
/**
 * \def PLED1_CFG
 * \brief Macro para Configurar el Pin relacionado a las funciones 
 * \ref blinky_update() \ref blinky_init() \ref blinky_set() .
 * \return nothing
 * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
 * <b><i> piero.jel@gmail.com </i></b>.
 * \version 01v01d01.
 * */
#define PLED1_CFG    TRISBbits.RB3



/**
 * \def LED1_CONFIG()
 * \brief Macro para configurar el Pin del LED1
 * \return nothing
 * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
 * <b><i> piero.jel@gmail.com </i></b>.
 * \version 01v01d01.
 * */
#define LED1_CONFIG() \
    PLED1_CFG = 0

/**
 * \def LED1_OFF()
 * \brief Macro para Apgar el Pin del LED1.
 * \return nothing
 * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
 * <b><i> piero.jel@gmail.com </i></b>.
 * \version 01v01d01.
 * */   
#define LED1_OFF() \
    PLED1 = 1    
/**
 * \def LED1_ON()
 * \brief Macro para Encender el Pin del LED1.
 * \return nothing.
 * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
 * <b><i> piero.jel@gmail.com </i></b>.
 * \version 01v01d01.
 * */
#define LED1_ON() \
    PLED1 = 0
        
/**
 * \def LED1_TOGGLE()
 * \brief Macro para Cambiar el estado del LED1, si esta encendido
 * lo apaga. De lo contrario lo enciende.
 * \return nothing.
 * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
 * <b><i> piero.jel@gmail.com </i></b>.
 * \version 01v01d01.
 * */
#define LED1_TOGGLE() \
    PLED1 ^= 1
    
/**
 * \def LED1(Action)
 * \brief Macro funcion para realizar una accion determinada
 * Sobre LED1
 * \param Action : mediante este argumento le indicamos la accion a realizar sobre SW1.
 * Este puede ser:
 *      + CONFIG    , Configurar el pin digital
 *      + ON        , encender el LED
 *      + OFF       , Apagar el LED
 *      + TOGGLE    , Cambiar de estado el LED
 * \return nothing.
 * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
 * <b><i> piero.jel@gmail.com </i></b>.
 * \version 01v01d01.
 * */
#define LED1(Action) LED1_##Action()

/**
* \fn void blinky_update_FSM(uint8_t nBlink);
* \details descricpion de tallada
* \brief funcion para actualizar o establecer la FSM blink. 
* \param nBlink : Mediante este argumento podemos establecer el blink
* o en su defecto solo actualizar la FSM.
*   + \ref blinky_UPDATE : solo se actualiza.
*   + Valor entre 0 y 7, establece el blink. Si pasamos un valor mayor 
*   este es casteado a un maximo de 7.
* \return nothing
* 
* \note nota sobre la funcion
* \warning concideraciones a tener en cuenta para manejar con precaucion
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
 * <b><i> piero.jel@gmail.com </i></b>.
 * \version 01v01d01.
*/
void blinky_update_FSM(uint8_t nBlink);


/**
 * \def blinky_update()
 * \brief Funcion para actualizar, la FSM del LED de Estado. 
 * \return nothing.
 * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
 * <b><i> piero.jel@gmail.com </i></b>.
 * \version 01v01d01.
 * */
#define blinky_update() \
    blinky_update_FSM(blinky_UPDATE)
  
/**
 * \def blinky_init()
 * \brief Funcion para Inicializar el Hardware relacionado al led
 * de bliny.
 * \return nothing.
 * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
 * <b><i> piero.jel@gmail.com </i></b>.
 * \version 01v01d01.
 * */
#define blinky_init() \
    LED1(CONFIG)
/**
 * \def blinky_set(Blink)
 * \brief Funcion para Establecer el Numero de Blink del Led.
 * \param Blink : El numero de Blink para la tarea led blink. 
 * \return nothing.
 * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
 * <b><i> piero.jel@gmail.com </i></b>.
 * \version 01v01d01.
 * */    
#define blinky_set( Blink ) \
    blinky_update_FSM(Blink)
    

/* 
 * 
 * --------------------------------------------------------------------------------------------------
 * 
 * ***********************************[ End,   led_status ]***********************************
 * 
 * --------------------------------------------------------------------------------------------------
 */

#if (__switch_ENABLE__ == 1)


#define switch_SW_UP()        PORTCbits.RC4
#define switch_SW_DOWN()      PORTCbits.RC5
#define switch_SW_ACEPTAR()   PORTCbits.RC6
#define switch_SW_VOLVER()    PORTCbits.RC7

/**
*\def switch_NOT_PUSH
* \details
* \brief Etiqueta que nos indica que el switch no fue presionado. Un valor 
* distinito a este nos indica que fue pulsado el mismo.
* \note
* \warning */
#define switch_NOT_PUSH     0xAA
#define switch_NOISE_TIME   10 /**<@brief Establecemos el tiempo considerado como ruido
para los Switch/pulsadores.*/
#define switch_Up        0
#define switch_Down      1
#define switch_Aceptar   2
#define switch_Volver    3

/**
* ****************************************************************************//** 
* \fn void switch_init(void); 
*
* \brief Funcion para Inicializar el Hardware necesario para trabajar con los 
* Switch o Pulsadores.
* \return nothing.
* \version 01v01d01.
* \note notq.
* \warning mensaje de "warning". 
* \date Sabado  24 de Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \par example :
<PRE>
void main(void)
{
// .. codigo para inicializar
  switch_init();
  
  while(1)
  {
    switch_update(0);
    //... code for loop
  }

}
</PRE>  
*********************************************************************************/
void switch_init(void);    

/**
* ****************************************************************************//** 
* \fn uint8_t switch_update(uint8_t nSw);
*
* \brief Funcion para Inicializar el Hardware necesario para trabajar con los 
* Switch o Pulsadores.
* \param nSw : Numero de Switch o pulsador, el cual se quiere acturlizar y 
* obtener su estado.
* \return 
*   + 0, se pulso el switch o pulsador
*   + switch_NOT_PUSH, el Pulsador encuestado no fue presionado.
* \version 01v01d01.
* \note nota.
* \warning mensaje de "warning". 
* \date Sabado  24 de Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>.
* \par meil
* <PRE> + <b><i> piero.jel@gmail.com </i></b></PRE>
* \par example :
<PRE>
void main(void)
{
// .. codigo para inicializar
  switch_init();
  
  while(1)
  {
    if(!switch_update(0))
    {
      // accion a ejecutar cuando se presione el SW
    }
    //... code for loop
  }

}
</PRE>  
*********************************************************************************/
uint8_t switch_update(uint8_t nSw);
#endif/* #if (__switch_ENABLE__ == 1) */



/* 
* --------------------------------------------------------------------------------------------------
* 
* ***********************************[ begin, lcd ]***********************************
* 
* --------------------------------------------------------------------------------------------------
* 
*/
/**
  * \fn void lcd_init(void);
  * \brief Funcion para realizar la inicializacion del modulo LCD y el 
  * Hardware necesario para el correcto funcionamiento del mismo.
  * \return nothing.
  * \note es una funcion que demora mas de 20 ms, ya que debe inicializar
  * un modulo externo.
  * \warning Esta funcion demora un tiempo, debemos tener los recaudos suficientes.
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
  * <b><i> piero.jel@gmail.com </i></b>.
  * \version 01v01d01. */
void lcd_init(void);
/**
  * \fn void lcd_putc(uint8_t cmd);
  * \brief Funcion para enviar un caracter al modulo LCD, en la direccion 
  * actual asiganada por el cursor. 
  * \param cmd : caracter, debe ser en format ASCI, imprimible.
  * \return nothing.
  * \note nota.
  * \warning Esta funcion demora un tiempo, debemos tener los recaudos suficientes.
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
  * <b><i> piero.jel@gmail.com </i></b>.
  * \version 01v01d01. */
void lcd_putc(uint8_t cmd);
/**
  * \fn void lcd_puts(const char *str);
  * \brief Funcion para enviar una cadena de caracter o string 
  * al modulo LCD, en la direccion actual asiganada por el cursor. 
  * \param str : Cadena de Caracteres o String.
  * \return nothing.
  * \note nota.
  * \warning La cadena de caracters debe contar con el caracter de terminacion '\0'.
  * Esta funcion demora un tiempo, debemos tener los recaudos suficientes.
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
  * <b><i> piero.jel@gmail.com </i></b>.
  * \version 01v01d01. */
void lcd_puts(const char *str);
/**
  * \fn void lcd_puts_xy(unsigned char posX,unsigned char posY,const char *msg);
  * \brief Funcion para enviar una cadena de caracter o string 
  * al modulo LCD, en una posicion 'x'(columna) e 'y'(fila).
  * \param posX : Posicion en 'x' donde inicia a imprimir el string.
  * \param posY : Posicion en 'y' (Fila, ) donde inicia a imprimir el string.
  * \param msg : Cadena de Caracteres o String.
  * \return nothing.
  * \note nota.
  * \warning La cadena de caracters debe contar con el caracter de terminacion '\0'.
  * Esta funcion demora un tiempo, debemos tener los recaudos suficientes.
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
  * <b><i> piero.jel@gmail.com </i></b>.
  * \version 01v01d01. */
void lcd_puts_xy(unsigned char posX,unsigned char posY,const char *msg);

/**
  * \fn void lcd_clear(void);
  * \brief Funcion para realizar un clrear, una limpieza de la pantalla.
  * \return nothing.
  * \note nota.
  * \warning Esta funcion demora un tiempo, debemos tener 
  * los recaudos suficientes.
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
  * <b><i> piero.jel@gmail.com </i></b>.
  * \version 01v01d01. */
void lcd_clear(void);
/**
  * \fn void lcd_Command(uint8_t cmd );
  * \brief Funcion para enviar un comando al modulo LCD.
  * \param cmd : comando que deseamos enviarle al modulo.
  * \return nothing.
  * \note nota.
  * \warning Esta funcion demora un tiempo, debemos tener 
  * los recaudos suficientes.
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
  * <b><i> piero.jel@gmail.com </i></b>.
  * \version 01v01d01. */
void lcd_Command(uint8_t cmd );
/*
void lcd_deco2digitos(unsigned char posX, unsigned char posY,\
    unsigned char dato);*/
/**
  * \fn void lcd_puts_saldo(uint8_t posX, uint8_t posY,uint16_t saldo);
  * \brief Funcion para inprimir el un saldo de hasta "99,99" en una posicion 
  * indicada, sobre la pantalla del LCD.  
  * \param posX : Posicion en 'x' donde inicia a imprimir el string.
  * \param posY : Posicion en 'y' (Fila, ) donde inicia a imprimir el string.
  * \param saldo : Saldo, es un entero de 16-Bits, la coma es colocada de forma
  * autoamtica considerando que el saldo debe ser dividido por 100.
  * \return nothing.
  * \note nota.
  * \warning En caso de el valor de saldo sea mayo a 9999, este es casteado
  * a ese valor. Esta funcion demora un tiempo,
  * debemos tener los recaudos suficientes.
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
  * <b><i> piero.jel@gmail.com </i></b>.
  * \version 01v01d01. */
void lcd_puts_saldo(uint8_t posX, uint8_t posY,\
    uint16_t saldo);

/* 
 * 
 * --------------------------------------------------------------------------------------------------
 * 
 * ***********************************[ End,   lcd ]***********************************
 * 
 * --------------------------------------------------------------------------------------------------
 */


/* 
* --------------------------------------------------------------------------------------------------
* 
* ***********************************[ begin, keypad ]***********************************
* 
* --------------------------------------------------------------------------------------------------
* 
*/

/** 
 * \def keypad_NOT_KEYPRESS 
 * \brief Etiqueta para identificar que no se presiono ninguna tecla.
 * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
  * <b><i> piero.jel@gmail.com </i></b>.
  * \version 01v01d01.
 */
#define keypad_NOT_KEYPRESS         0xFF

/**
* ****************************************************************************//** 
* \fn uint8_t keypad_update(void);
* \brief Funcion que actualiza la FSM (maquina de estado finita) de Teclado y 
* devuelve un dato establecido en el mapa de teclado, en caso de que la tecla 
* presionada corresponda con las coordenadas. Si ninguna tecla fue presionada
* esta retorna la etiqueta correspondiete.
*
* \return Devuelve el valor correspondiete a la coordenada "x;y" de la tecla 
* presionada, la cual se declaro en el mapa de teclado \ref keypad_map.
* En el caso que no se presiones ninguna tecla aun, o este precesando un posible
* evento de cambio de estado en el escaneo de las columnas, esta devuevle
* \ref keypad_NOT_KEYPRESS.
* \note nota.
* \warning mensaje de "warning". 
* \date Sabado 24 de Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
* <b><i> piero.jel@gmail.com </i></b>.
* \version 01v01d01.
* \par example :
<PRE>
// ingreso de valor por teclado
    Tecla = keypad_update();
    if( Tecla != keypad_NOT_KEYPRESS)
    {
        // procesamso el valor de la tecla presionada
    }
    return;
</PRE>  
*********************************************************************************/
uint8_t keypad_update(void);

/**
* ****************************************************************************//** 
* \fn void keypad_init(void);
* \brief Inicializa el harware necesario para el correcto funcionamiento del 
* teclado.
* \return nothing
* \note nota.
* \warning mensaje de "warning". 
* \date Sabado 24 de Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
* <b><i> piero.jel@gmail.com </i></b>.
* \par example :
<PRE>
// ingreso de valor por teclado
    if(keypad_update() != keypad_NOT_KEYPRESS)
    {
        tecla = keypad_update();
        timer_load(tecla);
    }
    return;
</PRE>  
*********************************************************************************/
void keypad_init(void);

/* 
 * 
 * --------------------------------------------------------------------------------------------------
 * 
 * ***********************************[ End,   keypad ]***********************************
 * 
 * --------------------------------------------------------------------------------------------------
 */

/* 
* --------------------------------------------------------------------------------------------------
* 
* ***********************************[ begin, apis para FSM principal ]***********************************
* 
* --------------------------------------------------------------------------------------------------
* 
*/

/*
 *-- Definimo los pines para El Acturador  
 * XXX:
    • RC4/D-/VM   
        ■ RC4 : Pin de ENTRADA (solo entrada) Digital "RC4", para que este funcione debemos configurar los registros corespondiente. UCON<3> = 0  y UCFG<3> = 1.
        PORTC<4> data input; disabled when USB module or on-chip transceiver are enabled.
        ■ D- : USB bus differential minus line output (internal transceiver). USB bus differential minus line input (internal transceiver).
        ■ VM : External USB transceiver VM input.
        
    • RC5/D+/VP   
        ■ RC5 : Pin de ENTRADA (solo entrada) Digital "RC5", para que este funcione debemos configurar los registros corespondiente. UCON<3> = 0  y UCFG<3> = 1. 
        PORTC<5> data input; disabled when USB module or on-chip transceiver are enabled.                                 
        ■ D+ : USB bus differential plus line output (internal transceiver). USB bus differential plus line input (internal transceiver).
        ■ VP : External USB transceiver VP input.

 */
#define ACTURADOR0_CFG     TRISEbits.RE0  /**<@brief Establecemos el macro para configurar el Pin cero del Actuador */
#define ACTURADOR0         LATEbits.LE0   /**<@brief Establecemos el macro para Habilitar/Deshabilitar el Pin cero del Actuador */
#define ACTURADOR1_CFG     TRISEbits.RE1  /**<@brief Establecemos el macro para configurar el Pin uno del Actuador */
#define ACTURADOR1         LATEbits.LE1   /**<@brief Establecemos el macro para Habilitar/Deshabilitar el Pin uno del Actuador */
#define ACTURADOR2_CFG     TRISEbits.RE2  /**<@brief Establecemos el macro para configurar el Pin dos del Actuador */
#define ACTURADOR2         LATEbits.LE2   /**<@brief Establecemos el macro para Habilitar/Deshabilitar el Pin dos del Actuador */
/**/
#define ACTUADOR_OFF       0xEE /**<@brief Macro para apagar el actuador, todas las salidas/pines */

/**
* ****************************************************************************//** 
* \fn void maquina_cafe_init(void);
* \brief Inicializa el harware necesario para el correcto funcionamiento de la 
* FSM principal del Proyecto. 
* \return nothing
* \note nota
* \warning Debe ser invocada fuera del loop principal.
* \date Sabado 24 de Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>
* <b><i> piero.jel@gmail.com </i></b>.
*********************************************************************************/
void maquina_cafe_init(void);
/**
* ****************************************************************************//** 
* \fn void maquina_cafe_update(void);
* \brief Actualiza la FSM principal del Proyecto. 
* \return nothing
* \note nota
* \warning Debe ser invocada Dentro del loop principal.
* \date Sabado 24 de Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>
* <b><i> piero.jel@gmail.com </i></b>.
*********************************************************************************/
void maquina_cafe_update(void);
/**
* ****************************************************************************//** 
* \fn void maquina_cafe_actuador(uint8_t action);
* \brief Funcion que Habilita un estado del Actuador, en funcion del 
* producto que debe crear.
* \return nothing
* \note nota
* \warning Debe ser invocada Dentro del loop principal.
* \date Sabado 24 de Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>
* <b><i> piero.jel@gmail.com </i></b>.
*********************************************************************************/
void maquina_cafe_actuador(uint8_t action);


/* 
 * 
 * --------------------------------------------------------------------------------------------------
 * 
 * ***********************************[ End,   apis para FSM principal ]***********************************
 * 
 * --------------------------------------------------------------------------------------------------
 */


/* 
* --------------------------------------------------------------------------------------------------
* 
* ***********************************[ begin, monedero ]***********************************
* 
* --------------------------------------------------------------------------------------------------
* 
*/
/*
 *-- Definimos el pin para El monedero */
#define MONEDERO        LATAbits.LA2  /**<@brief Establecemos el Pin que usaremos para Habilitar/Deshabilitar el Monedero */
#define MONEDERO_CFG    TRISAbits.RA2 /**<@brief Establecemos el macro para configurar el Pin que usaremos para Habilitar/Deshabilitar el Monedero */
/*
 *-- Definimo los pines para leer el monedero */
    /* RD0 -> $1*/
#define MONEDA0_CFG     TRISDbits.RD0   /**<@brief Establecemos el macro para configurar el Pin relacionada a la Moneda 0 del Monedero */
#define MONEDA0         PORTDbits.RD0   /**<@brief Establecemos el Pin que notificara la aceptacion del tipo de moneda 0*/
#define MONEDA0_SALDO   100 /* $1 */    /**<@brief Establecemos la Etiqueta que representa el saldo para la moneda 0 */
#define MONEDA0_INDEX   0               /**<@brief indice dentro de la tabla \ref tbl_moneda, que contiene el valor de la moneda 0 */
    /* RC2 -> $2*/
#define MONEDA1_CFG     TRISCbits.RC2   /**<@brief Establecemos el macro para configurar el Pin relacionada a la Moneda 1 del Monedero */
#define MONEDA1         PORTCbits.RC2   /**<@brief Establecemos el Pin que notificara la aceptacion del tipo de moneda 1*/
#define MONEDA1_SALDO   200 /* $2 */    /**<@brief Establecemos la Etiqueta que representa el saldo para la moneda 1 */
#define MONEDA1_INDEX   1               /**<@brief indice dentro de la tabla \ref tbl_moneda, que contiene el valor de la moneda 1 */
    /* RC1 -> $5*/
#define MONEDA2_CFG     TRISCbits.RC1   /**<@brief Establecemos el macro para configurar el Pin relacionada a la Moneda 2 del Monedero */
#define MONEDA2         PORTCbits.RC1   /**<@brief Establecemos el Pin que notificara la aceptacion del tipo de moneda 2*/
#define MONEDA2_SALDO   500 /* $5 */    /**<@brief Establecemos la Etiqueta que representa el saldo para la moneda 2 */
#define MONEDA2_INDEX   2               /**<@brief indice dentro de la tabla \ref tbl_moneda, que contiene el valor de la moneda 2 */
    /* RC0 -> $1*/
#define MONEDA3_CFG     TRISCbits.RC0   /**<@brief Establecemos el macro para configurar el Pin relacionada a la Moneda 3 del Monedero */
#define MONEDA3         PORTCbits.RC0   /**<@brief Establecemos el Pin que notificara la aceptacion del tipo de moneda 3*/
#define MONEDA3_SALDO   1000 /* $10 */  /**<@brief Establecemos la Etiqueta que representa el saldo para la moneda 3 */
#define MONEDA3_INDEX   3               /**<@brief indice dentro de la tabla \ref tbl_moneda, que contiene el valor de la moneda 3 */

/*-- etiquetas para el modulo Monedero */
#define MONEDERO_EMPTLY     0xFF      /**<@brief Etiqueta que se implementa para notificar que el monedero esta vacio, no paso ninguna moneda. */
#define TIME_WAIT_MONEDERO  10        /**<@brief tiempo estimado que mantine en nivel bajo el pin relacionado a la moneda detectada, en milisegundos */
#define MONEDERO_TIME_ENABLE    100   /**<@brief tiempo estimado que se mantendra habilitado el monederoa para esperar el ingreso de una moneda, 10- Segundos, en centenas de milisegundos */



  /**
* ****************************************************************************//** 
* \fn void monedero_init(void);
* \brief Funcion que Inicializa el hardware relacionado al manejo del monedero.
* \return nothing
* \note nota
* \warning Debe ser invocada Fuera del loop principal.
* \date Sabado 24 de Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>
* <b><i> piero.jel@gmail.com </i></b>.
*********************************************************************************/             
void monedero_init(void);
/**
* ****************************************************************************//** 
* \fn uint8_t monedero_update(void);
* \brief Funcion que Actualiza la FSM que maneja el monedero.
* \return Esta devuelve el indice relacionada a la moneda, el cual
* debemos usar para obtener el saldo desde la tabla \ref tbl_moneda. Para 
* esta implementacion.
* En caso de no detectar una moneda valida devuelbe la etiqueta \ref MONEDERO_EMPTLY.
* \note nota
* \warning Debe ser invocada Dentro del loop principal.
* \date Sabado 24 de Octubre, 2020.
* \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>
* <b><i> piero.jel@gmail.com </i></b>.
*********************************************************************************/
uint8_t monedero_update(void);


/* 
 * 
 * --------------------------------------------------------------------------------------------------
 * 
 * ***********************************[ End,   monedero ]***********************************
 * 
 * --------------------------------------------------------------------------------------------------
 */

/* 
 * 
* ==================================================================================================
* 
* ********************************[ begin, declaracion de Maros ]*********************************
* 
* ==================================================================================================
*/

/**
  * \def CONFIG_INTERRUPT()
  * \brief Macro para configurar las interupciones utilizadas en esta aplicaccion.
  * Configuracion de las Interrupciones del DISPOSITIVO 
  * 
  * \li Registro de Configuracion del Dispositivo RCON
  *  + RCON<IPEN> = 0  , Deshabilitasmo el uso de prioridad de interrupcion.
  *  
  * \li Registro de Configuracion de Interrupciones INTCON 
  *  + INTCON<GIE/GIEH>    = 1   , Habilitamos las Interrupciones Globaler
  *  + INTCON<PEIE/GIEL>   = 0   , Deshabilitamos la Interrupcion perifericas 
  *                             (Interrupciones de TMR0 no esta dentro del grupo de Periferias)
  * \return nothing.
  * \date Sabado 24 de Octubre, 2020.
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
  * <b><i> piero.jel@gmail.com </i></b>.
  */
#define CONFIG_INTERRUPT() \
{\
    RCONbits.IPEN = 0; /* Deshabilitamso el uso de prioridades para interupcion */\
    INTCONbits.GIE = 1; /*Habilitamos las Interupciones Globales */\
    INTCONbits.PEIE = 0; /* Deshabilitamos las Interupciones Perifericas */\
}    
    
/**
  *\def systick_capture(Type);
  * \brief macro funcion para capturar los tick actual del sistema
  * \param Type : Tipo de captura, este debe ser:
  *   \li \ref TICK_ms, captura de mili segundos.
  *   \li \ref TICK_100ms, captura de centenas de mili segundos.
  * \return tick actual del sistema
  * \date Sabado 24 de Octubre, 2020.
  * \author <b> JEL </b> - <i> Jesus Emanuel Luccioni </i>. 
  * <b><i> piero.jel@gmail.com </i></b>.
*/
#define systick_capture(Type) systic_acuTick[Type]




/* 
* 
* ==================================================================================================
* 
* ***********************************[ End,   macro funciones ]***********************************
* 
* ==================================================================================================
*/
